import axios from 'axios';

const api = axios.create({
  baseURL: '/api'
});

export const apiGet = async (endpoint, params = {}) => api.get(endpoint, { params });
export const apiPost = async (endpoint, params = {}) => api.post(endpoint, { ...params });
export const apiPut = async (endpoint, params = {}) => api.put(endpoint, { ...params });
export const apiPatch = async (endpoint, params = {}) => api.patch(endpoint, { ...params });
export const apiDelete = async (endpoint, params = {}) => api.delete(endpoint, { ...params });

export default { apiGet, apiPost, apiPut, apiDelete };
