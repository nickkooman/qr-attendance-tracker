import { Server, Model, Factory } from 'miragejs';

export const mockApi = new Server({
  models: {
    locations: Model
  },

  factories: {
    location: Factory.extend({
      name(index) {
        return `Highschool ${index}`;
      },

      year() {
        const min = 2019;
        const max = 2020;

        return Math.floor(Math.random() * (max - min + 1)) + min;
      }
    })
  },

  routes() {
    this.namespace = 'api';

    this.get('/locations');
    this.get('/locations/:id');
    this.post('/locations');
    this.patch('/locations/:id');
    this.delete('/locations/:id');
  },

  seeds(server) {
    server.create('locations', { name: 'Zeeland East Highschool', year: 2020 });
    server.create('locations', { name: 'Zeeland West Highschool', year: 2020 });
    server.create('locations', { name: 'Hamilton Highschool', year: 2020 });
    server.create('locations', { name: 'Saugatuck Highschool', year: 2020 });
    server.create('locations', { name: 'Black River Highschool', year: 2020 });
    server.createList('location', 10);
  }
});

export default mockApi;
