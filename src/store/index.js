import Vue from 'vue';
import Vuex from 'vuex';
import { apiGet, apiPost, apiPut, apiDelete } from '@/services/api';

Vue.use(Vuex);

export const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {},
  mutations: {
    mutate(state, { property, data }) {
      state[property] = data;
    }
  },
  actions: {
    async get({ commit }, { url, property }) {
      const { data } = await apiGet(url);

      if (data) commit('mutate', { property, data });

      return data;
    },

    async post({ commit }, { url, property }) {
      const { data } = await apiPost(url);

      if (data) commit('mutate', { property, data });

      return data;
    },

    async put({ commit }, { url, property }) {
      const { data } = await apiPut(url);

      if (data) commit('mutate', { property, data });

      return data;
    },

    async delete({ commit }, { url, property }) {
      const { data } = await apiDelete(url);

      if (data) commit('mutate', { property, data });

      return data;
    }
  }
});

export default store;
